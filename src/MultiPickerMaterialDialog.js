import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import { material } from 'react-native-typography';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialDialog from './MaterialDialog';

import colors from './colors';

export default class MultiPickerMaterialDialog extends Component {
  constructor(props) {
    super(props);

    const { items, selectedItems } = props;
    const selected = new Map();
    selectedItems.forEach((item) => {
      selected.set(item.value, true);
    });

    this.state = { selected, filteredItems: items, searchQuery: undefined };
  }

  onPressItem(value) {
    this.setState((prevState) => {
      const selected = new Map(prevState.selected);
      selected.set(value, !selected.get(value));
      return { selected };
    });
  }

  setSelectedItem(value) {
    const selected = new Map();
    selected.set(value, true);
    this.setState({ selected });
  }

  keyExtractor = item => String(item.value);

  numberOfVisibleItems = () => {
    if(this.state.searchQuery && this.state.searchQuery.length > 0) {
      return this.props.items.filter(item => item.label.toLowerCase().includes(this.state.searchQuery.toLowerCase())).length;
    }
    return this.props.items.length;
  };

  renderItem = ({ item }) => {
    if(this.state.searchQuery && !item.label.toLowerCase().includes(this.state.searchQuery.toLowerCase())) {
      return null;
    }
    return (
      <TouchableOpacity onPress={() => this.onPressItem(item.value)} style={{paddingRight: 40}}>
        <View style={styles.rowContainer}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={styles.iconContainer}>
              <Icon
                name={
                  this.state.selected.get(item.value)
                    ? 'check-box'
                    : 'check-box-outline-blank'
                }
                color={this.props.colorAccent}
                size={24}
              />
            </View>
            <Text style={material.subheading}>{item.label}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <MaterialDialog
        searchQuery={this.props.enableSearch ? this.state.searchQuery : undefined}
        onSearchTextChange={this.props.enableSearch ? (text) => {
          this.setState({searchQuery: text}, () => {
            this.setState({filteredItems: this.numberOfVisibleItems() === 0 ? [] : this.props.items});
            this.forceUpdate();
            this.render();
          });
        } : undefined}
        title={this.props.title}
        titleColor={this.props.titleColor}
        colorAccent={this.props.colorAccent}
        visible={this.props.visible}
        okLabel={this.props.okLabel}
        scrolled={this.props.scrolled}
        onOk={() => {
          this.setState({searchQuery: ''}, () => {
            this.forceUpdate();
            this.render();
          });
          return this.props.onOk({
            selectedItems: this.props.items.filter(item =>
              this.state.selected.get(item.value),
            ),
          });
        }
        }
        cancelLabel={this.props.cancelLabel}
        onCancel={() => {
          this.setState({searchQuery: ''}, () => {
            this.forceUpdate();
            this.render();
          });
          this.props.onCancel();
        }}
      >
        <FlatList
          ListEmptyComponent={(
            <Text
              style={{
                marginVertical: 8,
                marginHorizontal: 0,
                paddingVertical: 8,
                paddingHorizontal: 8,
                marginLeft: 0,
                textAlign: 'center',
                fontSize: 18,
                borderWidth: 4,
                borderColor: this.props.colorAccent,
                borderRadius: 8,
                overflow: 'hidden',
              }}
            >
              No results matching the term{'\n'}'{this.state.searchQuery}'
            </Text>
          )}
          data={this.state.filteredItems}
          extraData={this.state}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </MaterialDialog>
    );
  }
}

const styles = StyleSheet.create({
  rowContainer: {
    minHeight: 56,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  iconContainer: {
    marginRight: 16,
  },
});

MultiPickerMaterialDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedItems: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  titleColor: PropTypes.string,
  colorAccent: PropTypes.string,
  enableSearch: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
  cancelLabel: PropTypes.string,
  okLabel: PropTypes.string,
  scrolled: PropTypes.bool,
};

MultiPickerMaterialDialog.defaultProps = {
  selectedItems: [],
  title: undefined,
  titleColor: undefined,
  colorAccent: colors.androidColorAccent,
  cancelLabel: undefined,
  enableSearch: false,
  okLabel: undefined,
  scrolled: false,
};
