import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList
} from 'react-native';
import {material} from 'react-native-typography';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialDialog from './MaterialDialog';

import colors from './colors';

export default class SinglePickerMaterialDialog extends Component {
    flatList;
    scrollTarget;
    viewHeightByIndex;

    constructor(props) {
        super(props);

        const {items, selectedItem} = props;

        let selectedIndex;
        if (selectedItem != null) {
            selectedIndex = items.findIndex(
              item => item.value === selectedItem.value,
            );
        }

        if (selectedIndex !== undefined && selectedIndex >= 8 && props.visible) {
            console.warn('constructor scroll target');
            this.scrollTarget = {animated: false, index: selectedIndex, viewOffset: 0.5};
        }

        this.state = {selectedIndex, filteredItems: items, searchQuery: undefined};
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.selectedItem === undefined && this.props.selectedItem !== prevProps.selectedItem) {
            this.scrollTarget = undefined;
            this.clearSelection();
            return;
        }

        if (this.state.selectedIndex !== undefined && this.state.selectedIndex >= 0 && !prevProps.visible && this.props.visible) {
            this.scrollTarget = {animated: false, index: this.state.selectedIndex, viewOffset: 0.5};
        }
        try {
            if (this.props.selectedItem && (this.props.selectedItem.label === undefined || this.props.selectedItem.label === 'All') && prevState.selectedIndex > 0) {
                this.clearSelection();
            }
        } catch (e) {
            this.clearSelection();
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (state.selectedIndex === null && props.selectedItem && props.selectedItem.label === 'All') {
            return {
                ...state,
                selectedIndex: 0,
                selectedItem: undefined
            };
        }
        return null;
    }

    onPressItem(item) {

        const target = Object.keys(item).includes('id') ? 'id' : 'value';
        const valTarget = target === 'id' ? item.id : item.value;

        const {items, okOnItemSelect} = this.props;
        this.setState(() => {
            const selectedIndex = items.findIndex(item => item[target] === valTarget);
            const selectedItem = items[selectedIndex];
            return {selectedIndex, selectedItem, searchQuery: undefined};
        }, () => {
            if (okOnItemSelect) {
                this.setState({filteredItems: items, searchQuery: undefined}, () => {
                    this.forceUpdate();
                    this.render();
                });
                return this.props.onOk({
                    selectedItem: this.props.items[this.state.selectedIndex],
                });
            }
        });
    }

    keyExtractor = item => String(item.value + '_' + item.id);

    numberOfVisibleItems = () => {
        return (this.state.filteredItems || []).length;
    }

    renderItem = ({item, index}) => {
        const target = Object.keys(item).includes('id') ? 'id' : 'value';
        const valTarget = target === 'id' ? item.id : item.value;
        index = this.props.items.findIndex(item => item[target] === valTarget);
        return (
          <TouchableOpacity
            onPress={() => {
                if (index === this.state.selectedIndex) {
                    this.clearSelection();
                } else {
                    this.onPressItem(item)
                }
            }}
            onLayout={(event) => {
                if (this.viewHeightByIndex === undefined) {
                    this.viewHeightByIndex = new Map();
                }
                this.viewHeightByIndex.set(index, event.nativeEvent.layout.height);
            }}
          >
              <View style={styles.rowContainer}>
                  <View style={styles.iconContainer}>
                      <Icon
                        name={
                            index === this.state.selectedIndex
                              ? 'radio-button-checked'
                              : 'radio-button-unchecked'
                        }
                        color={this.props.colorAccent}
                        size={24}
                      />
                  </View>
                  <Text style={{...material.subheading, flex: 1}}>{item.label}</Text>
                  {index === this.state.selectedIndex && (this.props.showDeselect && !this.props.forceSelection) && (
                    <TouchableOpacity
                      activeOpacity={0.75}
                      style={{
                          justifyContent: 'center',
                          padding: 8,
                      }}
                      onPress={() => {
                          this.clearSelection();
                      }}
                    >
                        <Icon
                          name={'clear'}
                          color={this.props.colorAccent}
                          size={24}
                          style={{
                              textAlign: 'right'
                          }}
                        />
                    </TouchableOpacity>
                  )}
              </View>
          </TouchableOpacity>
        );
    };

    clearSelection() {
        if (this.props.forceSelection) return;
        this.scrollTarget = undefined;
        this.setState({
            searchQuery: '',
            filteredItems: this.props.items,
            selectedIndex: null,
            selectedItem: undefined,
        });
        this.props.onOk({
            selectedItem: {label: '', value: ''},
        });
    }

    filterItems(items) {
        return [...items].filter(item => item.label.toLowerCase().includes((this.state.searchQuery || '').toLowerCase()));
    }

    render() {
        const viewHeightByIndex = this.viewHeightByIndex || new Map();
        return (
          <MaterialDialog
            searchQuery={this.props.enableSearch ? this.state.searchQuery : undefined}
            onSearchTextChange={this.props.enableSearch ? (text) => {
                this.setState({searchQuery: text}, () => {
                    this.setState({filteredItems: this.filterItems(this.props.items)});
                    try {
                        this.flatList.scrollToIndex(
                          {
                              animated: false,
                              index: 0
                          }
                        )
                    } catch (e) {
                        // noop
                    }
                });
            } : undefined}
            title={this.props.title}
            titleColor={this.props.titleColor}
            colorAccent={this.props.colorAccent}
            visible={this.props.visible}
            okLabel={this.props.okLabel}
            scrolled={this.props.scrolled}
            onOk={() => {
                this.setState({
                    searchQuery: undefined,
                    selectedItem: this.props.items[this.state.selectedIndex]
                }, () => {
                    this.forceUpdate();
                    this.render();
                });
                return this.props.onOk({
                    selectedItem: this.props.items[this.state.selectedIndex],
                });
            }}
            okOnItemSelect={this.props.okOnItemSelect}
            cancelLabel={this.props.cancelLabel}
            onCancel={() => {
                this.setState({searchQuery: undefined}, () => {
                    this.forceUpdate();
                    this.render();
                });
                this.props.onCancel();
            }}
          >
              <FlatList
                onLayout={() => {
                    if (this.scrollTarget && this.flatList) {
                        this.flatList.scrollToIndex(this.scrollTarget);
                        this.scrollTarget = undefined;
                        this.flatList.flashScrollIndicators();
                    }
                }}
                ListEmptyComponent={(
                  <Text
                    style={{
                        marginVertical: 8,
                        marginHorizontal: 0,
                        paddingVertical: 8,
                        paddingHorizontal: 8,
                        marginLeft: 0,
                        textAlign: 'center',
                        fontSize: 18,
                        borderWidth: 4,
                        borderColor: this.props.colorAccent,
                        borderRadius: 8,
                        overflow: 'hidden',
                    }}
                  >
                      {this.props.items.length === 0 ? 'No items to display.' : `No results matching the term\n'${this.state.searchQuery}'`}
                  </Text>
                )}
                data={this.state.filteredItems}
                extraData={this.state}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
                getItemLayout={(data, index) => {
                    if (this.numberOfVisibleItems() === 1 && viewHeightByIndex.has(index)) {
                        ITEM_HEIGHT = viewHeightByIndex.get(index);
                        return {length: ITEM_HEIGHT, offset: 0, index};
                    }
                    let ITEM_HEIGHT = 54;
                    if (viewHeightByIndex.has(index)) {
                        ITEM_HEIGHT = viewHeightByIndex.get(index);
                    }
                    let offset = 0;
                    for (let i = 0; i < index; i++) {
                        if (viewHeightByIndex.has(i)) {
                            offset += viewHeightByIndex.get(i);
                        } else {
                            offset += ITEM_HEIGHT;
                        }
                    }
                    return {length: ITEM_HEIGHT, offset, index};
                }}
                ref={(ref) => {
                    this.flatList = ref;
                }}
              />
          </MaterialDialog>
        );
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        minHeight: 56,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconContainer: {
        marginRight: 16,
    },
});

SinglePickerMaterialDialog.propTypes = {
    visible: PropTypes.bool.isRequired,
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedItem: PropTypes.shape({
        value: PropTypes.any.isRequired,
        label: PropTypes.string.isRequired,
    }),
    title: PropTypes.string,
    titleColor: PropTypes.string,
    colorAccent: PropTypes.string,
    onCancel: PropTypes.func.isRequired,
    onOk: PropTypes.func.isRequired,
    cancelLabel: PropTypes.string,
    okLabel: PropTypes.string,
    scrolled: PropTypes.bool,
    okOnItemSelect: PropTypes.bool,
    enableSearch: PropTypes.bool,
    showDeselect: PropTypes.bool,
    forceSelection: PropTypes.bool,
};

SinglePickerMaterialDialog.defaultProps = {
    selectedItem: undefined,
    title: undefined,
    titleColor: undefined,
    colorAccent: colors.androidColorAccent,
    cancelLabel: undefined,
    okLabel: undefined,
    scrolled: false,
    enableSearch: false,
    showDeselect: false,
    forceSelection: false,
};
